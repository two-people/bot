from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.utils import executor
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import ChatActions
import zipfile
import io
import os
from datetime import datetime

from config import TOKEN


class UserStates(StatesGroup):
    wait_photos = State()
    wait_archive_name = State()


bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
users_files_dir ="users_files\\"


@dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    user_id = message.from_user.id
    try:
        os.mkdir(users_files_dir + str(user_id))
    except FileExistsError:
        pass

    await message.reply("Привет!\nЯ запаковщик домашек. Быстрей присылай фотки."
                        "\nДля большей информации отправь мне \\help ")
    await UserStates.wait_photos.set()


@dp.message_handler(commands=['help'], state="*")
async def help_command(message: types.Message):
    await message.reply("Отправь мне фотографии, а я отправлю zip файл с ними тебе в ответ!"
                        "\nЧтобы назвать файл, отправь \\name_file "
                        "\nЧтобы получить готовый архив, отправь \\archive ")


@dp.message_handler(commands=['name_file'], state="*")
async def name_file_command(message: types.Message):
    await message.reply("Как назовем файл с домашкой?")
    await UserStates.wait_archive_name.set()


@dp.message_handler(state=UserStates.wait_archive_name, content_types=types.ContentType.TEXT)
async def name_file_set(message: types.Message, state: FSMContext):
    archive_name = message.text
    if len(archive_name) > 100:
        await message.reply("Слишком длинное имя, введите другое")
        return
    await state.update_data(archive_name=archive_name)
    await message.reply("Задано имя "+archive_name)
    await UserStates.wait_photos.set()


@dp.message_handler(content_types=types.ContentType.PHOTO, state="*")
async def photo_getting(message: types.Message):
    user_dir = users_files_dir+str(message.from_user.id)
    if not os.path.exists(user_dir):
        os.mkdir(user_dir)

    photo_id = message.photo[-1].file_id
    photo = await bot.get_file(photo_id)
    photo_path = photo.file_path
    photo_name = (str(datetime.now()) + ".jpg").replace(" ","").replace(":","")
    loaded_file: io.BytesIO = await bot.download_file(photo_path)
    with open(user_dir+"\\"+photo_name, "wb") as f:
        f.write(loaded_file.read())
    await message.reply("Фото получено")


@dp.message_handler(content_types=types.ContentType.DOCUMENT, state="*")
async def document_getting(message: types.Message):
    user_dir = users_files_dir + str(message.from_user.id)
    if not os.path.exists(user_dir):
        os.mkdir(user_dir)

    file_id = message.document.file_id
    file_name = message.document.file_name
    file = await bot.get_file(file_id)
    file_path = file.file_path
    loaded_file: io.BytesIO = await bot.download_file(file_path)
    with open(user_dir + "\\" + file_name.replace(" ",""), "wb") as f:
        f.write(loaded_file.read())
    await message.reply("Документ получен")


@dp.message_handler(commands=['archive'], state="*")
async def arhcive_command(message: types.Message, state: FSMContext):
    await message.reply("Готовлю архив, подождите")
    user_id = message.from_user.id
    await bot.send_chat_action(user_id, ChatActions.UPLOAD_DOCUMENT)
    user_dir = users_files_dir + str(user_id)
    user_files = os.listdir(user_dir)
    if len(user_files) == 0:
        await message.reply("Вы не загрузили фото для архивации")
        return

    archive_name = str(datetime.now()).replace(" ","").replace(":","")
    user_data = await state.get_data()
    if "archive_name" not in user_data:
        await message.reply("Вы не задали имя для архива, поэтому используется текущее время")
    else:
        archive_name = user_data["archive_name"]
    zip_file_path = user_dir + "\\" + archive_name + ".zip"

    user_zip = zipfile.ZipFile(zip_file_path, 'w')
    for file_path in user_files:
        user_zip.write(user_dir+"\\"+file_path, arcname=file_path, compress_type=zipfile.ZIP_DEFLATED)
        os.remove(user_dir+"\\"+file_path)
    user_zip.close()
    zip_file = types.input_file.InputFile(zip_file_path)
    await bot.send_document(user_id, zip_file, caption='Файлы успешно заархивированы')
    os.remove(zip_file_path)


if __name__ == '__main__':
    print("Бот запущен")
    try:
        os.mkdir(users_files_dir)
    except FileExistsError:
        pass
    executor.start_polling(dp)


